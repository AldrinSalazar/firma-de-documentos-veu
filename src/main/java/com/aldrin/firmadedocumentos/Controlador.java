package com.aldrin.firmadedocumentos;


import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Controlador implements View.OnClickListener, DatePickerDialog.OnDateSetListener{

    Principal vista;
    Modelo modelo;

    public Controlador(Principal p){
        vista = p;
        modelo = new Modelo();
    }

    private void mensaje(String s){
        Toast.makeText(vista, s, Toast.LENGTH_SHORT).show();
    }

    private boolean validarCampos(){
        if(vista.firmantes.getText().toString().equals("")){
            mensaje("Ingresar firmante");
            return false;
        }

        if(vista.destinatario.getText().toString().equals("")){
            mensaje("Ingresar Destinatario");
            return false;
        }

        if(vista.asunto.getText().toString().equals("")){
            mensaje("Ingresar asunto");
            return false;
        }

        if(vista.fechaSeleccionada.getText().toString().equals("")){
            mensaje("Seleccionar fecha");
            return false;
        }

        return true;
    }

    private void clickGenerar(){
        if(validarCampos()){

            modelo.setAsunto(vista.asunto.getText().toString());
            modelo.setCopias(vista.copias.getText().toString());
            modelo.setDepartamento(vista.departamento.getSelectedItem().toString());
            modelo.setDestinatario(vista.destinatario.getText().toString());
            modelo.setFecha(vista.fechaSeleccionada.getText().toString());
            modelo.setPersonasFirmantes(vista.firmantes.getText().toString());
            modelo.setD(Modelo.departamentos.values()[vista.departamento.getSelectedItemPosition()]);


            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    modelo.enviarABD();
                }
            }, "Enviar");
            t.start();
            try{
                t.join();
            }catch (Exception e){
                e.printStackTrace();
            }

            if(modelo.error){
                vista.codigo.setText("Error");
                vista.referencia.setText("Error");
            }else{
                vista.codigo.setText(modelo.getCodigo());
                vista.referencia.setText(modelo.getReferencia());
            }
        }
    }

    private void clickFecha(){
        vista.dp.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", new Locale("es_ES"));
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        vista.fechaSeleccionada.setText(df.format(newDate.getTime()));
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonGenerar)
            clickGenerar();
        else
            clickFecha();
    }


}
