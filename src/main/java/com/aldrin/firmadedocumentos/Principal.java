package com.aldrin.firmadedocumentos;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Principal extends AppCompatActivity {

    EditText firmantes;
    EditText destinatario;
    EditText asunto;
    EditText copias;
    EditText fechaSeleccionada;

    Button generar;
    Button fecha;

    Spinner departamento;

    TextView referencia;
    TextView codigo;

    DatePickerDialog dp;

    Controlador c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        c = new Controlador(this);

        firmantes = (EditText) findViewById(R.id.editTextFirmante);
        destinatario = (EditText) findViewById(R.id.editTextDestinatario);
        asunto = (EditText) findViewById(R.id.editTextAsunto);
        copias = (EditText) findViewById(R.id.editTextCopias);
        fechaSeleccionada = (EditText) findViewById(R.id.editTextFecha);

        generar = (Button) findViewById(R.id.buttonGenerar);
        generar.setOnClickListener(c);

        fecha = (Button) findViewById(R.id.buttonSeleccionFecha);
        fecha.setOnClickListener(c);

        referencia = (TextView) findViewById(R.id.textViewReferencia);
        codigo = (TextView) findViewById(R.id.textViewCodigo);

        departamento = (Spinner) findViewById(R.id.spDepartamento);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.departamentos_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        departamento.setAdapter(adapter);


        Calendar calendar = Calendar.getInstance();
        dp = new DatePickerDialog(this, c, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }
}
