package com.aldrin.firmadedocumentos;


import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Random;

public class Modelo {
    public enum departamentos{
        PR,
        SG,
        SAC,
        SO,
        SC,
        STR,
        SIS,
        SD,
        SA,
        SF,
        II
    }

    private String personasFirmantes;
    private String departamento;
    private departamentos d;
    private String destinatario;
    private String asunto;
    private String copias;
    private String fecha;

    private String referencia;
    private String codigo;

    private Random r = new Random();

    public boolean error = false;

    public Modelo(){}

    private String generarCodigo(){
        Hashids hashids = new Hashids("HASH SECRETO");
        codigo = hashids.encode(r.nextInt(900000000));
        return codigo;
    }

    private String generarReferencia(){
        String[] f = fecha.split("-");
        referencia = "VEU-"+d.name()+"-"+f[1]+"/"+f[2]+"("+r.nextInt(100)+")";
        return referencia;
    }

    public void enviarABD(){
        /*vanguardia-estudiantil.org.ve/documentos/anfaiuabawq.php?
        firmante=Esta&
        departamento=es&
        destinatario=una&
        asunto=prueba&
        fecha=plz&
        copias=hola&
        codigo=lololol&
        identificador=equisde&
        pw=*****
            */

        try {
            personasFirmantes = URLEncoder.encode(personasFirmantes, "UTF-8");
            departamento = URLEncoder.encode(departamento, "UTF-8");
            destinatario = URLEncoder.encode(destinatario, "UTF-8");
            asunto = URLEncoder.encode(asunto, "UTF-8");
            fecha = URLEncoder.encode(fecha, "UTF-8");
            copias = URLEncoder.encode(copias, "UTF-8");

            String parametros = "?firmante="+personasFirmantes+
                    "&departamento="+departamento+
                    "&destinatario="+destinatario+
                    "&asunto="+asunto+
                    "&fecha="+fecha+
                    "&copias="+copias+
                    "&codigo="+generarCodigo()+
                    "&identificador="+generarReferencia()+
                    "&pw=******";
		
		//TODO: Activar SSL
            URL url = new URL("http://vanguardia-estudiantil.org.ve/documentos/anfaiuabawq.php"+parametros);
            URLConnection connection = url.openConnection();
            connection.connect();
            connection.getInputStream();

        } catch (MalformedURLException e1) {
            error = true;
            e1.printStackTrace();
        } catch (IOException e2) {
            error = true;
            e2.printStackTrace();
        }
    }

    public void setPersonasFirmantes(String personasFirmantes) {
        this.personasFirmantes = personasFirmantes;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public void setCopias(String copias) {
        this.copias = copias;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setD(departamentos d) {
        this.d = d;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getReferencia() {
        return referencia;
    }
}
